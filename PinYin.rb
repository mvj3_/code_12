require 'rubygems'
require 'ruby-pinyin'
puts PinYin.of_string('姚尚朗').join(',')
puts PinYin.of_string('姚尚朗',true).join(',')
puts PinYin.permlink('姚尚朗')
puts PinYin.permlink('姚尚朗 is 我')


=begin ===OUTPUT===
YAO,SHANG,LANG
YAO2,SHANG4,LANG3
yao-shang-lang
yao-shang-lang--is-wo
=end